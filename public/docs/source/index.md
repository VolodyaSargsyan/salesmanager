---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Companies
<!-- START_78434ef06aa01ac1b68001536f353e53 -->
## Create a new Company

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST "http://localhost/api/company/create" \
    -H "Content-Type: application/json" \
    -d '{"name":"aFbV1pkDMxXiihnr","country_id":2,"address":"FSpY9sWnhGqUNZ7S","address2":"ubinlSX4QiS7i6QO","city":"9JMKRsknMuLxBR0w","state":"wcP98ta9cOM9Jgte","type":"prJONNlWI4eOt8Qy","website":"A1S5dZpP0oBofZOm","bank_info":"6BK4wzkJOmgg4VjY","observations":"8f0sfjP03KXyRYLd"}'

```

```javascript
const url = new URL("http://localhost/api/company/create");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "name": "aFbV1pkDMxXiihnr",
    "country_id": 2,
    "address": "FSpY9sWnhGqUNZ7S",
    "address2": "ubinlSX4QiS7i6QO",
    "city": "9JMKRsknMuLxBR0w",
    "state": "wcP98ta9cOM9Jgte",
    "type": "prJONNlWI4eOt8Qy",
    "website": "A1S5dZpP0oBofZOm",
    "bank_info": "6BK4wzkJOmgg4VjY",
    "observations": "8f0sfjP03KXyRYLd"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "name": "ZCompA",
    "country_id": "4",
    "address": "drinciceva",
    "type": "test_company",
    "updated_at": "2019-04-13 12:36:39",
    "created_at": "2019-04-13 12:36:39",
    "id": 64
}
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "The name field is required."
        ]
    }
}
```
> Example response (404):

```json
{
    "error": "Resource not found."
}
```

### HTTP Request
`POST api/company/create`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | The Company Name
    country_id | integer |  required  | ID of the country that company belongs
    address | string |  required  | Company Address
    address2 | string |  optional  | optional Second part of company address
    city | string |  optional  | optional The city that company is situated
    state | string |  optional  | optional State that city belongs
    type | string |  required  | Company type
    website | string |  optional  | optional Company's website
    bank_info | string |  optional  | optional Company's bank information
    observations | string |  optional  | optional Any additional information

<!-- END_78434ef06aa01ac1b68001536f353e53 -->


<!-- START_cc0eb700bd44d07437d9e8f6199777ba -->
## Update a Company

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
[Updates a company, you need to send only the updated fields. The other ones will remain the same.]

> Example request:

```bash
curl -X PUT "http://localhost/api/company/{company}/update" \
    -H "Content-Type: application/json" \
    -d '{"name":"cYr3Vp5m3FQ6QP9h","country_id":12,"address":"ojyfhOLNlnlxx3FL","address2":"oDhskragdsRo6eaH","city":"DfvXyejbp5cEUXgz","state":"VwkKUkofmzAPFya9","type":"2jDw7a15Euwtcjhn","website":"IphbRb0ACm8USKuY","bank_info":"93YfejM9wJArm3cl","observations":"709Jth5uPkxpOJCT"}'

```

```javascript
const url = new URL("http://localhost/api/company/{company}/update");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "name": "cYr3Vp5m3FQ6QP9h",
    "country_id": 12,
    "address": "ojyfhOLNlnlxx3FL",
    "address2": "oDhskragdsRo6eaH",
    "city": "DfvXyejbp5cEUXgz",
    "state": "VwkKUkofmzAPFya9",
    "type": "2jDw7a15Euwtcjhn",
    "website": "IphbRb0ACm8USKuY",
    "bank_info": "93YfejM9wJArm3cl",
    "observations": "709Jth5uPkxpOJCT"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Company successfully updated."
}
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
> Example response (403):

```json
{}
```
> Example response (404):

```json
{
    "error": "Resource not found."
}
```
> Example response (422):

```json
{
    "message": "No data was sent to update the company."
}
```

### HTTP Request
`PUT api/company/{company}/update`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  optional  | optional The Company Name
    country_id | integer |  optional  | optional ID of the country that company belongs
    address | string |  optional  | optional Company Address
    address2 | string |  optional  | optional Second part of company address
    city | string |  optional  | optional The city that company is situated
    state | string |  optional  | optional State that city belongs
    type | string |  optional  | optional Company type
    website | string |  optional  | optional Company's website
    bank_info | string |  optional  | optional Company's bank information
    observations | string |  optional  | optional Any additional information

<!-- END_cc0eb700bd44d07437d9e8f6199777ba -->


<!-- START_d9edfa68b7fb821c87a5a5063e6a9541 -->
## Delete a company

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE "http://localhost/api/company/{company}/delete" 
```

```javascript
const url = new URL("http://localhost/api/company/{company}/delete");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Company successfully deleted."
}
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
> Example response (403):

```json
{}
```
> Example response (404):

```json
{
    "error": "Resource not found."
}
```

### HTTP Request
`DELETE api/company/{company}/delete`


<!-- END_d9edfa68b7fb821c87a5a5063e6a9541 -->


<!-- START_e25813400d2a781bec95e2a9948b08b3 -->
## api/getCountries
> Example request:

```bash
curl -X GET -G "http://localhost/api/getCountries" 
```

```javascript
const url = new URL("http://localhost/api/getCountries");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/getCountries`


<!-- END_e25813400d2a781bec95e2a9948b08b3 -->


<!-- START_6cd57914b0109bfb2bb88e7f90086383 -->
## api/getCountryByID/{id}
> Example request:

```bash
curl -X GET -G "http://localhost/api/getCountryByID/{id}" 
```

```javascript
const url = new URL("http://localhost/api/getCountryByID/{id}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/getCountryByID/{id}`


<!-- END_6cd57914b0109bfb2bb88e7f90086383 -->


<!-- START_83764a2de1a941a0a3cbae52bba9776e -->
## api/companies
> Example request:

```bash
curl -X GET -G "http://localhost/api/companies" 
```

```javascript
const url = new URL("http://localhost/api/companies");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/companies`


<!-- END_83764a2de1a941a0a3cbae52bba9776e -->


<!-- START_fafd6b085baab15e5b8e0bbc2457767c -->
## Return all companies that belongs to the current user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
[This method return a list of companies]

> Example request:

```bash
curl -X GET -G "http://localhost/api/getCompanyForUpdate/{id}" 
```

```javascript
const url = new URL("http://localhost/api/getCompanyForUpdate/{id}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "permission_id": 40,
    "user_id": 1,
    "company_id": 61,
    "role": "manager",
    "company_name": "ZCompA",
    "address": "drinciceva",
    "address2": null,
    "city": null,
    "state": null,
    "country_id": 4,
    "type": "test_company",
    "website": null,
    "bank_info": null,
    "observations": null
}
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/getCompanyForUpdate/{id}`


<!-- END_fafd6b085baab15e5b8e0bbc2457767c -->


<!-- START_eecadedde31ce35a25a1a9e50385b225 -->
## api/getCompaniesCountriesRelation
> Example request:

```bash
curl -X GET -G "http://localhost/api/getCompaniesCountriesRelation" 
```

```javascript
const url = new URL("http://localhost/api/getCompaniesCountriesRelation");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/getCompaniesCountriesRelation`


<!-- END_eecadedde31ce35a25a1a9e50385b225 -->


#Contacts
<!-- START_04eba74f2aa3e41cd987f078bae29851 -->
## Show the form for creating a new resource.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST "http://localhost/api/contact/create" \
    -H "Content-Type: application/json" \
    -d '{"name":"ng0N8o9U49b8neBA","company_id":9,"email":"9sYuuR913GLdXcGl","phone":"RQ59VDlAVBr1mBJh"}'

```

```javascript
const url = new URL("http://localhost/api/contact/create");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "name": "ng0N8o9U49b8neBA",
    "company_id": 9,
    "email": "9sYuuR913GLdXcGl",
    "phone": "RQ59VDlAVBr1mBJh"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "name": "Contact A",
    "company_id": "4",
    "email": "test@emaill.com",
    "phone": "0123456789",
    "updated_at": "2019-04-13 12:36:39",
    "created_at": "2019-04-13 12:36:39",
    "id": 10
}
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "The email field is required."
        ]
    }
}
```
> Example response (404):

```json
{
    "error": "Resource not found."
}
```

### HTTP Request
`POST api/contact/create`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  optional  | The contact name
    company_id | integer |  optional  | Company ID that contact belongs
    email | string |  optional  | Contact's email
    phone | string |  optional  | Contact's phone

<!-- END_04eba74f2aa3e41cd987f078bae29851 -->


<!-- START_46d3167b10bb97755374d44e4a46b03d -->
## Update the specified resource in storage.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT "http://localhost/api/company/{company}/contact/{contact}/update" \
    -H "Content-Type: application/json" \
    -d '{"name":"uzR99i37gyi0KQpy","email":"ChTLeVXkzdNI7uXg","phone":"iW3d4dWYp5ym0d2R"}'

```

```javascript
const url = new URL("http://localhost/api/company/{company}/contact/{contact}/update");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "name": "uzR99i37gyi0KQpy",
    "email": "ChTLeVXkzdNI7uXg",
    "phone": "iW3d4dWYp5ym0d2R"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Contact successfully updated."
}
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "The email field is required."
        ]
    }
}
```
> Example response (404):

```json
{
    "error": "Resource not found."
}
```

### HTTP Request
`PUT api/company/{company}/contact/{contact}/update`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  optional  | The contact name
    email | string |  optional  | Contact's email
    phone | string |  optional  | Contact's phone

<!-- END_46d3167b10bb97755374d44e4a46b03d -->


<!-- START_ee23d2adebf2bbf3d36c1d098a630061 -->
## Deletes a contact

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE "http://localhost/api/company/{company}/contact/{contact}/delete" 
```

```javascript
const url = new URL("http://localhost/api/company/{company}/contact/{contact}/delete");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Contact successfully deleted."
}
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
> Example response (403):

```json
{}
```
> Example response (404):

```json
{
    "error": "Resource not found."
}
```

### HTTP Request
`DELETE api/company/{company}/contact/{contact}/delete`


<!-- END_ee23d2adebf2bbf3d36c1d098a630061 -->


#Orders
<!-- START_828414d7e56dceae7697ad8b34b8a7c4 -->
## Create a new Order

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST "http://localhost/api/company/{company}/order/create" 
```

```javascript
const url = new URL("http://localhost/api/company/{company}/order/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "buyer_company_id": "10",
    "supplier_company_id": "6",
    "order_reference": "order#123",
    "total": "3500",
    "invoice_number": "N123",
    "currency_id": "20",
    "incoterm": "12",
    "payment_terms": "terms",
    "quantity": "200",
    "measure": "2",
    "status": "1",
    "observations": "obs",
    "order_date": "2019-06-25",
    "company_id": "7",
    "updated_at": "2019-06-25 19:22:26",
    "created_at": "2019-06-25 19:22:26",
    "id": 14
}
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "The total field is required."
        ]
    }
}
```

### HTTP Request
`POST api/company/{company}/order/create`


<!-- END_828414d7e56dceae7697ad8b34b8a7c4 -->


<!-- START_a91dc99270e750fc74a5134730e3b7cc -->
## Update a Order

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
[Updates a order, you need to send only the updated fields. The other ones will remain the same.]

> Example request:

```bash
curl -X PUT "http://localhost/api/company/{company}/order/{order}/update" 
```

```javascript
const url = new URL("http://localhost/api/company/{company}/order/{order}/update");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Order successfully updated."
}
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
> Example response (403):

```json
{}
```
> Example response (404):

```json
{
    "error": "Resource not found."
}
```
> Example response (422):

```json
{
    "message": "No data was sent to update the order."
}
```

### HTTP Request
`PUT api/company/{company}/order/{order}/update`


<!-- END_a91dc99270e750fc74a5134730e3b7cc -->


<!-- START_b11fead883d991278a7dc1c8d1d4d1f5 -->
## Delete a order

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE "http://localhost/api/company/{company}/order/{order}/delete" 
```

```javascript
const url = new URL("http://localhost/api/company/{company}/order/{order}/delete");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Order successfully deleted."
}
```
> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
> Example response (403):

```json
{}
```
> Example response (404):

```json
{
    "error": "Resource not found."
}
```

### HTTP Request
`DELETE api/company/{company}/order/{order}/delete`


<!-- END_b11fead883d991278a7dc1c8d1d4d1f5 -->




