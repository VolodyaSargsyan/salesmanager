
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Bootstrap
 |--------------------------------------------------------------------------
 |
 | First, we will load all of the "core" dependencies for Spark which are
 | libraries such as Vue and jQuery. This also loads the Spark helpers
 | for things such as HTTP calls, forms, and form validation errors.
 |
 | Next, we'll create the root Vue application for Spark. This will start
 | the entire application and attach it to the DOM. Of course, you may
 | customize this script as you desire and load your own components.
 |
 */

require('spark-bootstrap');

require('./components/bootstrap');

/*Including VueRouter*/
import VueRouter from 'vue-router'
Vue.use(VueRouter)
/*Including VueRouter*/

/*Including VueAxios*/
import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);
/*Including VueAxios*/

/*Including Bootstrap-vue*/
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)
/*Including Bootstrap_vue*/

/*Date Formatting*/
import moment from 'moment'
Vue.config.productionTip = false
Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('YYYY-MM-DD')
    }
});


/*For Flash Messages*/
window.events = new Vue();
window.flash = function(message) {
    window.events.$emit('flash',message);
}
Vue.component('flash', require('./FlashMsg.vue'));
Vue.component('order-list', require('./components/Company/Order/List'));
/*For Flash Messages*/

/*Routes*/
const routes = [
    /*Routes For Companies*/
    { path: '/', name: 'companies', component: require('./components/Company/List.vue') },
    { path: '/create-company', name: 'createCompany', component: require('./components/Company/Create.vue') },
    { path: '/update-company/:id', name: 'updateCompany', component: require('./components/Company/Update.vue') },
    { path: '/details-company/:id', name: 'detailsCompany', component: require('./components/Company/Details.vue') },
    { path: '/create-contact/:id', name: 'createContact', component: require('./components/Company/CreateContact.vue') },
    { path: '/update-contact/:id', name: 'updateContact', component: require('./components/Company/UpdateContact.vue') },
    { path: '/create-order/:id', name: 'createOrder', component: require('./components/Company/Order/CreateOrder.vue') },
    { path: '/update-order/:id', name: 'updateOrder', component: require('./components/Company/Order/UpdateOrder.vue') },
    /** Error Handling Routes */
    { path: '/403', name: '403', component: require('./components/Errors/403.vue') },
    { path: '/404', name: '404', component: require('./components/Errors/404.vue') },
    { path: '*', redirect: '/404' }
];

const router = new VueRouter({
    routes
})
/*Routes*/


var app = new Vue({
    mixins: [require('spark')],
    router
});
