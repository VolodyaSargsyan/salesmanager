@extends('spark::layouts.app')

@section('content')
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row justify-content-center">
            <flash message=""></flash>
            <router-view></router-view>
        </div>
    </div>
@endsection
