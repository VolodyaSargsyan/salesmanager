<?php


return [

    /*
    |--------------------------------------------------------------------------
    | General Messages in SalesManager
    |--------------------------------------------------------------------------
    |
    | The following language lines contain default messages used in the application
    |
    */
    'company_updated'       => 'Company successfully updated.',
    'company_updated_empty' => 'No data was sent to update the company.',
    'company_updated_error' => 'Company data was not updated.',
    'company_deleted'       => 'Company successfully deleted.',
    'company_deleted_error' => 'Company data was not deleted.',
    'no_permission'         => 'You don\'t have permission on this resource.',
    'contact_updated_empty' => 'No data was sent to update the contact.',
    'contact_updated'       => 'Contact successfully updated.',
    'contact_updated_error' => 'Contact data was not updated.',
    'contact_deleted'       => 'Contact successfully deleted.',
    'contact_deleted_error' => 'Contact was not deleted.',
    'contact_wrong_company' => 'Contact doesn\'t belong this company.',
    'order_updated_empty'   => 'No data was sent to update the order.',
    'order_updated'         => 'Order successfully updated.',
    'order_updated_error'   => 'Order data was not updated.',
    'order_deleted'         => 'Order successfully deleted.',
    'order_deleted_error'   => 'Order was not deleted.',
    'order_wrong_company'   => 'Order doesn\'t belong this company.',
    'payment_updated_empty' => 'No data was sent to update the payment.',
    'payment_updated'       => 'Payment successfully updated.',
    'payment_updated_error' => 'Order data was not updated.',
    'payment_wrong_order'   => 'Payment doesn\'t belong to this order.',
    'payment_deleted'       => 'Payment successfully deleted.',
    'payment_deleted_error' => 'Payment was not deleted.'
];
