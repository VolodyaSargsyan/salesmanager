Sales Manager Software

## Requirements ##
* PHP 7.1 or higher
* PostGreSQL

## How to install and run Sales Manager ##
* Install all packages `composer install` and `npm install`
* Copy `.env.example` to `.env`
* Configure the database
```
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=***********
DB_USERNAME=***********
DB_PASSWORD=**********
```
* Run all migrations `php artisan migrate --seed`
* Run the server: `php artisan serve`