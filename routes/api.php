<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register the API routes for your application as
| the routes are automatically authenticated using the API guard and
| loaded automatically by this application's RouteServiceProvider.
|
*/

Route::group([
    'middleware' => 'auth:api'
], function () {
    /**
     * Company CRUD Routes
     */
    Route::post('/company/create', 'CompanyController@create');
    Route::put('/company/{company}/update', 'CompanyController@update');
    Route::delete('/company/{company}/delete', 'CompanyController@destroy');
    Route::get('/company/{company}', 'CompanyController@getCompany');
    Route::get('/companies', 'CompanyController@companies');

    /**
     * Contact CRUD Routes
     */
    Route::post('/contact/create', 'ContactController@create');
    Route::put('/company/{company}/contact/{contact}/update', 'ContactController@update');
    Route::delete('/company/{company}/contact/{contact}/delete', 'ContactController@destroy');

    /**
     * Order CRUD Routes
     */
    Route::post('/company/{company}/order/create', 'OrdersController@create');
    Route::put('/company/{company}/order/{order}/update', 'OrdersController@update');
    Route::delete('/company/{company}/order/{order}/delete', 'OrdersController@destroy');
    Route::get('/company/{company}/orders', 'OrdersController@getOrders');
    Route::get('/order/{order}', 'OrdersController@getOrder');
    Route::get('/orders', 'OrdersController@getAllOrders');
    Route::get('/company/{company}/order/{order}/payments', 'OrdersController@getPayments');

    /**
     * Payments Routes
     */
    Route::post('/payment/create', 'PaymentController@create');
    Route::put('/company/{company}/order/{order}/payment/{payment}/update', 'PaymentController@update');
    Route::delete('/company/{company}/order/{order}/payment/{payment}/delete', 'PaymentController@destroy');

    /**
     * Countries Data
     */
    Route::get('/getCountries', 'CompanyController@getCountries');
    Route::get('/getCountryByID/{id}', 'CompanyController@getCountrysById');

    /**
     * User Data Routes
     */
    Route::get('/getCompanies', 'CompanyController@getCompanies');
    Route::get('/getCurrencies', 'CompanyController@getCurrencies');

    /**
     * Companies Data Routes
     */
    Route::get('/getCompanyForUpdate/{company}', 'CompanyController@getCompanyForEdit');
    Route::get('/getContactForUpdate/{id}', 'ContactController@getContactForEdit');
    Route::get('/getCompaniesCountriesRelation', 'CompanyController@getCompaniesCountriesRelation');
});
