<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('supplier_company_id')->index();
            $table->unsignedBigInteger('buyer_company_id')->index();
            $table->string('order_reference', 100);
            $table->string('invoice_number', 100);
            $table->float('total');
            $table->unsignedInteger('currency_id');
            $table->unsignedInteger('incoterm');
            $table->string('payment_terms', 100);
            $table->float('quantity');
            $table->float('measure');
            $table->unsignedInteger('status');
            $table->string('observations')->nullable();
            $table->timestamp('order_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
