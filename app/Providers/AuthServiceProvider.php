<?php

namespace App\Providers;

use App\Models\Permission;
use App\Models\UserCompany;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('hasPermission', function ($user, $resource) {
           return UserCompany::where('user_id', $user->id)
               ->where('company_id', $resource->id)
               ->first();
        });
    }
}
