<?php


namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class PaymentRepository extends Repository
{
    public function model()
    {
        return 'App\Models\Payment';
    }
}