<?php


namespace App\Repositories;


use Bosnadev\Repositories\Eloquent\Repository;

class OrderRepository extends Repository
{
    public function model()
    {
        return 'App\Models\Order';
    }
}