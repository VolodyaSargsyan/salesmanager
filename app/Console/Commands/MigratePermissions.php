<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\Permission;
use App\Models\UserCompany;
use Illuminate\Console\Command;

class MigratePermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate Old Permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $permissions = Permission::where('resource_type', Company::class)->get();
        foreach ($permissions as $permission) {
            UserCompany::create([
                'user_id'       => $permission->user_id,
                'company_id'    => $permission->resource_id,
                'permission'    => $permission->role
            ]);
        }
    }
}
