<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Webpatser\Countries\Countries;

/**
 * Class Order
 * @package App\Models
 * @property integer $id
 * @property-read Company $company
 * @property-read Collection $payments
 */
class Order extends Model
{
    use SoftDeletes;

    protected $fillable = ['buyer_company_id', 'supplier_company_id', 'order_reference','total', 'invoice_number',
        'currency_id', 'incoterm', 'payment_terms', 'quantity', 'measure', 'status', 'observations', 'order_date',
        'company_id'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Company::class, 'supplier_company_id');
    }

    public function buyer()
    {
        return $this->belongsTo(Company::class, 'buyer_company_id');
    }

    public function countries()
    {
        return $this->belongsTo(Countries::class, 'currency_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
