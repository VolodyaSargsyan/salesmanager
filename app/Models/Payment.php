<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Payment
 * @package App\Models
 * @property integer $order_id
 * @property Carbon $date
 * @property boolean $is_received
 * @property string $comments
 * @property float $amount
 * @property-read Order $order
 */
class Payment extends Model
{
    use SoftDeletes;

    protected $table = 'payments';

    protected $casts = [
        'is_received'   => 'boolean',
        'date'          => 'datetime:Y-m-d',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date',
    ];

    protected $fillable = ['order_id', 'date', 'is_received', 'comments', 'amount'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
