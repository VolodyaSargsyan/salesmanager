<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model
{
    protected $table = 'users_companies';

    protected $fillable = ['user_id', 'company_id', 'permission'];
}