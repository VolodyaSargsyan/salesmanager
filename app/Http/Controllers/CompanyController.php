<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCompany;
use App\Http\Requests\UpdateCompany;
use App\Repositories\CompanyRepository;
use App\Models\Company;
use App\Models\Contact;
use Auth;
use Illuminate\Support\Facades\Gate;
use Lang;
use Laravel\Spark\Spark;
use Illuminate\Http\Response;
use Webpatser\Countries\Countries;

/**
 * Class CompanyController
 *
 * @package App\Http\Controllers
 * @group Companies
 */
class CompanyController extends Controller
{
    protected $company;

    /**
     * Create a new controller instance.
     *
     * @param $company CompanyRepository Company Repository Instance
     * @return void
     */
    public function __construct(CompanyRepository $company)
    {
        $this->middleware('auth');
        $this->company = $company;
    }

    /**
     * Create a new Company
     *
     * @param CreateCompany $request Request Data
     * @authenticated
     * @response {
     *  "name": "ZCompA",
     *  "country_id": "4",
     *  "address": "drinciceva",
     *  "type": "test_company",
     *  "updated_at": "2019-04-13 12:36:39",
     *  "created_at": "2019-04-13 12:36:39",
     *  "id": 64
     * }
     * @response 401 {
     *   "message": "Unauthenticated."
     * }
     * @response 422 {
     *  "message": "The given data was invalid.",
     *  "errors": {
     *      "name": [
     *          "The name field is required."
     *      ]
     *  }
     * }
     * @response 404 {
     *  "error": "Resource not found."
     * }
     * @return Company
     */
    public function create(CreateCompany $request)
    {
        /** @var Company $company New company created*/
        $company = $this->company->create($request->all());

        /** Create a permission for the newly created company */
        Spark::interact(UserCompaniesController::class . '@store', [
            [
                'user_id'       => Auth::user()->id,
                'company_id'    => $company->id,
                'role'          => 'manager'
            ]
        ]);

        return $company;
    }

    /**
     * Update a Company
     *
     * [Updates a company, you need to send only the updated fields. The other ones will remain the same.]
     *
     * @param  UpdateCompany $request
     * @param  Company  $company
     * @authenticated
     * @response {
     *  "message": "Company successfully updated."
     * }
     * @response 401 {
     *   "message": "Unauthenticated."
     * }
     * @response 403 {
     *   "message":  "message": "You don't have permission on this resource."
     * }
     * @response 404 {
     *  "error": "Resource not found."
     * }
     * @response 422 {
     *  "message": "No data was sent to update the company."
     * }
     * @return Response
     */
    public function update(UpdateCompany $request, Company $company)
    {
        /** Verify user permission */
        if (Gate::denies('hasPermission', $company)) {
            return response()->json([
                'message'   => Lang::get('messages.no_permission')
            ], 403);
        }

        if (empty($request->all())) {
            return response()->json([
                'message'   => Lang::get('messages.company_updated_empty')
            ], 422);
        }

        $updated = $this->company->update($request->all(), $company->id);
        if ($updated) {
            return response()->json([
                'message'   => Lang::get('messages.company_updated')
            ]);
        }

        return response()->json([
            'message'   => Lang::get('messages.company_updated_error')
        ], 422);
    }

    /**
     * Delete a company
     *
     * @param  Company  $company
     * @authenticated
     * @response {
     *  "message": "Company successfully deleted."
     * }
     * @response 401 {
     *   "message": "Unauthenticated."
     * }
     * @response 403 {
     *   "message":  "message": "You don't have permission on this resource."
     * }
     * @response 404 {
     *  "error": "Resource not found."
     * }
     * @return Response
     */
    public function destroy(Company $company)
    {
        /** Verify user permission */
        if (Gate::denies('hasPermission', $company)) {
            return response()->json([
                'message'   => Lang::get('messages.no_permission')
            ]);
        }

        $permission = Auth::user()->hasPermission($company);
        if ($permission) {
            try {
                $permission->delete();
                $this->company->delete($company->id);

                return response()->json([
                    'message'   => Lang::get('messages.company_deleted')
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'message'   => Lang::get('messages.company_deleted_error')
                ], 422);
            }
        }

        return response()->json([
            'message'   => Lang::get('messages.company_deleted_error')
        ], 422);
    }

    /**
     * Return all companies that belongs to the current user
     *
     * [This method return a list of companies]
     *
     * @authenticated
     * @response {
     *    "permission_id": 40,
     *    "user_id": 1,
     *    "company_id": 61,
     *    "role": "manager",
     *    "company_name": "ZCompA",
     *    "address": "drinciceva",
     *    "address2": null,
     *    "city": null,
     *    "state": null,
     *    "country_id": 4,
     *    "type": "test_company",
     *    "website": null,
     *    "bank_info": null,
     *    "observations": null
     * }
     * @response 401 {
     *   "message": "Unauthenticated."
     * }
     * @return mixed
     */
    public function getCompanyForEdit(Company $company)
    {
        /** Verify user permission */
        if (Gate::denies('hasPermission', $company)) {
            return response()->json([
                'message'   => Lang::get('messages.no_permission')
            ], 403);
        }

        $company->load(['contact']);

        return $company;
    }
    public function companies()
    {
        Auth::user()->load(['companies.country']);
        return Auth::user()->companies;
    }

    public function getCompanies(){
        Auth::user()->load(['companies']);
        $companies = Auth::user()->companies;
        return $companies->pluck('name', 'id');
    }

    public function getCurrencies(){
        return Countries::all()->pluck('currency_code', 'id');
    }

    public function getCompaniesCountriesRelation(){
        return Company::with('country')->orderByDesc('updated_at')->get();
    }

    public function getCountries(){
        $countries = Countries::all()->sortBy('name');
        return $countries->values()->pluck('name', 'id')->all();
    }

    public function getCountrysById($id){
        return Countries::find($id);
    }

    public function show()
    {
        return view('company.index');
    }

    public function getCompany(Company $company)
    {
        /** Verify user permission */
        if (Gate::denies('hasPermission', $company)) {
            return response()->json([
                'message'   => Lang::get('messages.no_permission')
            ], 403);
        }

        return $company->load(['country']);
    }
}
