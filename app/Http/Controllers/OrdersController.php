<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrder;
use App\Http\Requests\UpdateOrder;
use App\Models\Company;
use App\Models\Order;
use App\Repositories\OrderRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Lang;

/**
 * Class OrdersController
 *
 * @package App\Http\Controllers
 * @group Orders
 */
class OrdersController extends Controller
{
    protected $orderRepo;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->middleware('auth');
        $this->orderRepo = $orderRepo;
    }

    /**
     * Create a new Order
     *
     * @param CreateOrder $request Request Data
     * @authenticated
     * @response {
     *  "buyer_company_id": "10",
     *  "supplier_company_id": "6",
     *  "order_reference": "order#123",
     *  "total": "3500",
     *  "invoice_number": "N123",
     *  "currency_id": "20",
     *  "incoterm": "12",
     *  "payment_terms": "terms",
     *  "quantity": "200",
     *  "measure": "2",
     *  "status": "1",
     *  "observations": "obs",
     *  "order_date": "2019-06-25",
     *  "company_id": "7",
     *  "updated_at": "2019-06-25 19:22:26",
     *  "created_at": "2019-06-25 19:22:26",
     *  "id": 14
     * }
     * @response 401 {
     *   "message": "Unauthenticated."
     * }
     * @response 422 {
     *  "message": "The given data was invalid.",
     *  "errors": {
     *      "name": [
     *          "The total field is required."
     *      ]
     *  }
     * }
     * @return Order
     */
    public function create(CreateOrder $request)
    {
        return $this->orderRepo->create($request->all());
    }

    /**
     * Update a Order
     *
     * [Updates a order, you need to send only the updated fields. The other ones will remain the same.]
     *
     * @param  UpdateOrder $request
     * @param  Company  $company
     * @param  Order $order
     * @authenticated
     * @response {
     *  "message": "Order successfully updated."
     * }
     * @response 401 {
     *   "message": "Unauthenticated."
     * }
     * @response 403 {
     *   "message":  "message": "You don't have permission on this resource."
     * }
     * @response 404 {
     *  "error": "Resource not found."
     * }
     * @response 422 {
     *  "message": "No data was sent to update the order."
     * }
     * @return Response
     */
    public function update(UpdateOrder $request, Company $company, Order $order)
    {
        /** Verify user permission */
        if (Gate::denies('hasPermission', $company)) {
            return response()->json([
                'message'   => Lang::get('messages.no_permission')
            ], 403);
        }

        if (empty($request->all())) {
            return response()->json([
                'message'   => Lang::get('messages.order_updated_empty')
            ], 422);
        }

        $updated = $this->orderRepo->update($request->all(), $order->id);
        if ($updated) {
            return response()->json([
                'message'   => Lang::get('messages.order_updated')
            ]);
        }

        return response()->json([
            'message'   => Lang::get('messages.order_updated_error')
        ], 422);
    }

    /**
     * Delete a order
     *
     * @param Company $company
     * @param Order $order
     * @authenticated
     * @response {
     *  "message": "Order successfully deleted."
     * }
     * @response 401 {
     *   "message": "Unauthenticated."
     * }
     * @response 403 {
     *   "message":  "message": "You don't have permission on this resource."
     * }
     * @response 404 {
     *  "error": "Resource not found."
     * }
     * @return Response
     */
    public function destroy(Company $company, Order $order)
    {
        if (!$order->company) {
            return response()->json([
                'message'   => Lang::get('messages.order_wrong_company')
            ]);
        }

        /** Verify user permission */
        if (Gate::denies('hasPermission', $company)) {
            return response()->json([
                'message'   => Lang::get('messages.no_permission')
            ]);
        }

        try {
            $this->orderRepo->delete($order->id);

            return response()->json([
                'message'   => Lang::get('messages.order_deleted')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => Lang::get('messages.order_deleted_error')
            ], 422);
        }
    }

    /**
     * Get all orders that belongs to a company
     *
     * @param Company $company
     * @authenticated
     * @response   {
     *   "id": 1,
     *   "supplier_company_id": 2,
     *   "buyer_company_id": 3,
     *   "order_reference": "12365",
     *   "invoice_number": "68974452",
     *   "total": "12500",
     *   "currency_id": 8,
     *   "incoterm": 8,
     *   "payment_terms": "test",
     *   "quantity": "2",
     *   "measure": "2",
     *   "status": 4,
     *   "observations": null,
     *   "order_date": "2019-08-14 00:00:00",
     *   "created_at": "2019-08-20 17:10:06",
     *   "updated_at": "2019-08-20 17:10:06",
     *   "deleted_at": null,
     *   "company_id": 3
     *   }
     * @response 401 {
     *   "message": "Unauthenticated."
     * }
     * @response 403 {
     *   "message":  "message": "You don't have permission on this resource."
     * }
     * @response 404 {
     *  "error": "Resource not found."
     * }
     * @return Company
     */
    public function getOrders(Company $company)
    {
        /** Verify user permission */
        if (Gate::denies('hasPermission', $company)) {
            return response()->json([
                'message'   => Lang::get('messages.no_permission')
            ]);
        }

        $company->load(['orders.supplier.countries', 'orders.buyer.countries' , 'orders.countries']);

        return $company;
    }

    public function getOrder(Order $order)
    {
        return $order;
    }

    /**
     * Get All user orders
     */
    public function getAllOrders()
    {
        return response()->json(Auth::user()->orders());
    }

    public function getPayments(Company $company, Order $order)
    {
        /** Verify user permission */
        if (Gate::denies('hasPermission', $company)) {
            return response()->json([
                'message'   => Lang::get('messages.no_permission')
            ]);
        }

        return $order->payments;
    }
}
