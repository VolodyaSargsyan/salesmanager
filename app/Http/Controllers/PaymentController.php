<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePayment;
use App\Http\Requests\UpdatePayment;
use App\Models\Company;
use App\Models\Order;
use App\Models\Payment;
use App\Repositories\PaymentRepository;
use Illuminate\Support\Facades\Gate;
use Lang;

class PaymentController extends Controller
{
    /**
     * @var PaymentRepository
     */
    private $paymentRepo;

    public function __construct(PaymentRepository $paymentRepo)
    {
        $this->middleware('auth');
        $this->paymentRepo = $paymentRepo;
    }

    public function create(CreatePayment $request)
    {
        return $this->paymentRepo->create($request->all());
    }

    public function update(UpdatePayment $request, Company $company, Order $order, Payment $payment)
    {
        /** Verify user permission */
        if (Gate::denies('hasPermission', $company)) {
            return response()->json([
                'message'   => Lang::get('messages.no_permission')
            ], 403);
        }

        if ($payment->order->id != $order->id) {
            return response()->json([
                'message'   => Lang::get('messages.payment_wrong_order')
            ]);
        }

        if (empty($request->all())) {
            return response()->json([
                'message'   => Lang::get('messages.payment_updated_empty')
            ], 422);
        }

        $updated = $this->paymentRepo->update($request->all(), $payment->id);
        if ($updated) {
            return response()->json([
                'message'   => Lang::get('messages.payment_updated')
            ]);
        }

        return response()->json([
            'message'   => Lang::get('messages.payment_updated_error')
        ], 422);
    }

    public function destroy(Company $company, Order $order, Payment $payment)
    {
        /** Verify user permission */
        if (Gate::denies('hasPermission', $company)) {
            return response()->json([
                'message'   => Lang::get('messages.no_permission')
            ]);
        }

        if ($payment->order->id != $order->id) {
            return response()->json([
                'message'   => Lang::get('messages.payment_wrong_order')
            ]);
        }

        try {
            $this->paymentRepo->delete($payment->id);

            return response()->json([
                'message'   => Lang::get('messages.payment_deleted')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => Lang::get('messages.order_deleted_error')
            ], 422);
        }
    }

}
