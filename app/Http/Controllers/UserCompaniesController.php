<?php

namespace App\Http\Controllers;

use App\Repositories\PermissionRepository;
use Illuminate\Http\Response;

class UserCompaniesController extends Controller
{
    /**
     * @var PermissionRepository
     */
    private $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * Store a new permission
     *
     * @param $data array
     * @return Response
     */
    public function store($data)
    {
        return $this->permissionRepository->create([
            'user_id'       => $data['user_id'],
            'company_id'    => $data['company_id'],
            'permission'    => $data['role']
        ]);
    }

}
