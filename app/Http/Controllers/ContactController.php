<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateContact;
use App\Http\Requests\UpdateContact;
use App\Models\Company;
use App\Models\Contact;
use Illuminate\Support\Facades\Gate;
use App\Repositories\ContactRepository;
use Illuminate\Support\Facades\Response;
use Lang;

/**
 * Class ContactController
 * @package App\Http\Controllers
 * @group Contacts
 */
class ContactController extends Controller
{
    protected $contactRepo;

    public function __construct(ContactRepository $contactRepo)
    {
        $this->middleware('auth');
        $this->contactRepo = $contactRepo;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CreateContact $request
     * @authenticated
     * @response {
     *  "name": "Contact A",
     *  "company_id": "4",
     *  "email": "test@emaill.com",
     *  "phone": "0123456789",
     *  "updated_at": "2019-04-13 12:36:39",
     *  "created_at": "2019-04-13 12:36:39",
     *  "id": 10
     * }
     * @response 401 {
     *   "message": "Unauthenticated."
     * }
     * @response 422 {
     *  "message": "The given data was invalid.",
     *  "errors": {
     *      "name": [
     *          "The email field is required."
     *      ]
     *  }
     * }
     * @response 404 {
     *  "error": "Resource not found."
     * }
     * @return Contact
     */
    public function create(CreateContact $request)
    {
        /** @var Contact $company New company created*/
        $contact = $this->contactRepo->create($request->all());

        return $contact;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateContact $request
     * @authenticated
     * @param Company $company
     * @param  Contact $contact
     * @response {
     *  "message": "Contact successfully updated."
     * }
     * @response 401 {
     *   "message": "Unauthenticated."
     * }
     * @response 422 {
     *  "message": "The given data was invalid.",
     *  "errors": {
     *      "name": [
     *          "The email field is required."
     *      ]
     *  }
     * }
     * @response 404 {
     *  "error": "Resource not found."
     * }
     * @return Response
     */
    public function update(UpdateContact $request, Company $company, Contact $contact)
    {
        /** Verify user permission */
        if (Gate::denies('hasPermission', $company)) {
            return response()->json([
                'message'   => Lang::get('messages.no_permission')
            ], 403);
        }

        if (empty($request->all())) {
            return response()->json([
                'message'   => Lang::get('messages.contact_updated_empty')
            ], 422);
        }

        $updated = $this->contactRepo->update($request->all(), $contact->id);
        if ($updated) {
            return response()->json([
                'message'   => Lang::get('messages.contact_updated')
            ]);
        }

        return response()->json([
            'message'   => Lang::get('messages.contact_updated_error')
        ], 422);
    }

    /**
     * Deletes a contact
     *
     * @param Company $company
     * @param Contact $contact
     * @authenticated
     * @response {
     *  "message": "Contact successfully deleted."
     * }
     * @response 401 {
     *   "message": "Unauthenticated."
     * }
     * @response 403 {
     *   "message":  "message": "You don't have permission on this resource."
     * }
     * @response 404 {
     *  "error": "Resource not found."
     * }
     * @return Response
     */
    public function destroy(Company $company, Contact $contact)
    {
        if (!$contact->company) {
            return response()->json([
                'message'   => Lang::get('messages.contact_wrong_company')
            ]);
        }

        /** Verify user permission */
        if (Gate::denies('hasPermission', $company)) {
            return response()->json([
                'message'   => Lang::get('messages.no_permission')
            ]);
        }

        try {
            $this->contactRepo->delete($contact->id);

            return response()->json([
                'message'   => Lang::get('messages.contact_deleted')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => Lang::get('messages.contact_deleted_error')
            ], 422);
        }
    }

    public function getContactForEdit($id)
    {
        return Contact::find($id);
    }

}
