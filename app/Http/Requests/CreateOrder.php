<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id'            => 'required|integer|exists:companies,id,deleted_at,NULL',
            'buyer_company_id'      => 'required|integer|exists:companies,id,deleted_at,NULL',
            'supplier_company_id'   => 'required|integer|exists:companies,id,deleted_at,NULL',
            'order_reference'       => 'required|string',
            'total'                 => 'required|numeric',
            'invoice_number'        => 'required|string',
            'currency_id'           => 'required|integer',
            'incoterm'              => 'required|integer',
            'payment_terms'         => 'required|string',
            'quantity'              => 'required|integer|min:0',
            'measure'               => 'required|numeric',
            'status'                => 'required|numeric',
            'observations'          => 'nullable|string',
            'order_date'            => 'required|date_format:Y-m-d'
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'buyer_company_id.exists'       => 'Buyer does not exists in our system.',
            'supplier_company_id.exists'    => 'Supplier does not exists in our system.',
            'company_id.exists'             => 'Company does not exists in our system.'
        ];
    }
}
