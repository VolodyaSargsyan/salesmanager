<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePayment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'      => 'integer|exists:orders,id',
            'date_format'   => 'date_format:Y-m-d',
            'is_received'   => 'boolean',
            'comments'      => 'string',
            'amount'        => 'numeric'
        ];
    }
}
