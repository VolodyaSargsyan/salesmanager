<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateCompany
 * @package App\Http\Requests
 * @bodyParam name string optional The Company Name
 * @bodyParam country_id integer optional ID of the country that company belongs
 * @bodyParam address string optional Company Address
 * @bodyParam address2 string optional Second part of company address
 * @bodyParam city string optional The city that company is situated
 * @bodyParam state string optional State that city belongs
 * @bodyParam type string optional Company type
 * @bodyParam website string optional Company's website
 * @bodyParam website string optional Company's website
 * @bodyParam bank_info string optional Company's bank information
 * @bodyParam observations string optional Any additional information
 */
class UpdateCompany extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'string',
            'country_id'    => 'integer',
            'address'       => 'string',
            'address2'      => 'string|nullable',
            'city'          => 'string|nullable',
            'state'         => 'string|nullable',
            'type'          => 'string',
            'website'       => 'string|nullable',
            'bank_info'     => 'string|nullable',
            'observations'  => 'string|nullable'
        ];
    }
}
