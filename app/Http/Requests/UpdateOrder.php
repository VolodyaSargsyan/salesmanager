<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id'            => 'nullable|integer|exists:companies,id,deleted_at,NULL',
            'buyer_company_id'      => 'nullable|integer|exists:companies,id,deleted_at,NULL',
            'supplier_company_id'   => 'nullable|integer|exists:companies,id,deleted_at,NULL',
            'order_reference'       => 'nullable|string',
            'total'                 => 'nullable|numeric',
            'invoice_number'        => 'nullable|string',
            'currency_id'           => 'nullable|integer',
            'incoterm'              => 'nullable|integer',
            'payment_terms'         => 'nullable|string',
            'quantity'              => 'nullable|integer|min:0',
            'measure'               => 'nullable|numeric',
            'status'                => 'nullable|numeric',
            'observations'          => 'nullable|string',
            'order_date'            => 'nullable|date_format:Y-m-d'
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'buyer_company_id.exists'       => 'Buyer does not exists in our system.',
            'supplier_company_id.exists'    => 'Supplier does not exists in our system.',
            'company_id.exists'             => 'Company does not exists in our system.'
        ];
    }
}
