<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateContact
 * @package App\Http\Requests
 * @bodyParam name string The contact name
 * @bodyParam email string Contact's email
 * @bodyParam phone string Contact's phone
 */
class UpdateContact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name'          => 'string|nullable',
            'email'         => 'nullable|email',
            'phone'         => 'nullable|string|unique:contacts,phone,' . $this->contact->id
        ];
    }
}
