<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CreateContact
 * @package App\Http\Requests
 * @bodyParam name string The contact name
 * @bodyParam company_id integer Company ID that contact belongs
 * @bodyParam email string Contact's email
 * @bodyParam phone string Contact's phone
 */
class CreateContact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|string',
            'company_id'    => 'required|integer|exists:companies,id,deleted_at,NULL',
            'email'         => 'required|email',
            'phone'         => 'nullable|string|unique:contacts,phone'
        ];
    }
}
