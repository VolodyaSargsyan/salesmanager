<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateCompany
 * @package App\Http\Requests
 * @bodyParam name string required The Company Name
 * @bodyParam country_id integer required ID of the country that company belongs
 * @bodyParam address string required Company Address
 * @bodyParam address2 string optional Second part of company address
 * @bodyParam city string optional The city that company is situated
 * @bodyParam state string optional State that city belongs
 * @bodyParam type string required Company type
 * @bodyParam website string optional Company's website
 * @bodyParam website string optional Company's website
 * @bodyParam bank_info string optional Company's bank information
 * @bodyParam observations string optional Any additional information
 */
class CreateCompany extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|string',
            'country_id'    => 'required|integer',
            'address'       => 'required|string',
            'address2'      => 'string|nullable',
            'city'          => 'string|nullable',
            'state'         => 'string|nullable',
            'type'          => 'string|required',
            'website'       => 'string|nullable',
            'bank_info'     => 'string|nullable',
            'observations'  => 'string|nullable'
        ];
    }
}
